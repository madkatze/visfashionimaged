var DatePickers = $('.datepicker');
var Sliders     = $('.slider');

$(document).ready(function() {
    if (DatePickers.length > 0) {
        DatePickers.datepicker({ 'format': 'dd.mm.yyyy' });
    }
    
    if (Sliders.length > 0) {
        Sliders.slider();
    }
});

/*
 * 
 * ShoppingCart listing functions
 * 
 * 
 */

function setupShoppingCartWidget(productId) {
    var cartQuantity    = $('#_cartProdQuantity');
    var cartQuantityInc = $('#_cartProdQuantityInc');
    var cartQuantityDec = $('#_cartProdQuantityDec');
    var cartBuy         = $('#_cartProdBuy');
    
    if ($('#_cartProdQuantity').val() === "") {
        $('#_cartProdQuantity').val(1);
    }
    
    cartQuantityInc.click(function() {
        cartQuantity.val(parseInt(cartQuantity.val()) + 1);
    });

    cartQuantityDec.click(function() {
        if (parseInt(cartQuantity.val()) - 1 > 0) {
            cartQuantity.val(parseInt(cartQuantity.val()) - 1);
        }
    });
    
    /**
     * Add product to shopping cart
     */
    cartBuy.click(function() {
        ajaxRequest('shopping_cart_add', {
            params: {
                'productId': productId
            },
            method: 'POST',
            data: {
                size: $('#prodSize').val(),
                quantity: parseInt(cartQuantity.val())
            },
            success: (function(data, status) {
                if (data.errno === 0) {
                    cartQuantity.val(data.quantity);
                    flashMessage('success', 'Продуктът е добавен успешно!');
                } else {
                    ajaxErrorhandler(data.errno);
                }
            }),
            fail: (function(request, status, error) {
                httpErrorHandler(request, status, error);
            })
        });
    });
}

function setupShoppingCartIndex() {
    var cartEmptyDialog    = $('#_cartEmptyDialog');
    var cartEmptyCancel    = $('#_cartEmptyCancel');
    var cartEmptyConfirm   = $('#_cartEmptyConfirm');
    var cartUpdate          = $('#_cartButtonUpdate');
    var cartEmpty           = $('#_cartButtonEmpty');
    var cartDrop            = $('._cartProdDrop');


    cartDrop.click(function() {
        // Lazy way to store information about product
        var productData     = $(this).attr('data-product').split('_');
        var elementParents  = $(this).parent().parent();
    
        ajaxRequest('shopping_cart_del', {
            params: {
                'productId': productData[0],    // Id of the product
                'size': productData[1]          // Size of the product
            },
            method: 'GET',
            data: {},
            success: (function(data, status) {
                if (data.errno === 0) {
                    elementParents.remove();
                    flashMessage('success', 'Продуктът е премахнат успешно!');
                    
                    if ($('#_cartList tbody tr').length === 0) {
                        $('table#_cartList').remove();
                        $('button[id*="_cartButton"]').remove();
                    }
                } else {
                    ajaxErrorhandler(data.errno);
                }
            }),
            fail: (function(request, status, error) {
                httpErrorHandler(request, status, error);
            })
        });
    });
    
    cartUpdate.click(function() {
        /*
         * Selector:
         * $('#_cartList tbody tr').toArray()[(each)].children[3].children[0].value
         */
        var productList = $('#_cartList tbody tr').toArray();
        var productData = new Array();
        
        productList.every(function(element, index, array) {
            var data = {
                'id': parseInt(productList[index].children[6].children[0].attributes.item('data-product').value.split('_')[0]),
                'size': parseInt(productList[index].children[6].children[0].attributes.item('data-product').value.split('_')[1]),
                'quantity': parseInt(element.children[3].children[0].value)
            };
            
            productData.push(data);
            
            return true;
        });
        
        ajaxRequest('shopping_cart_update', {
            params: {},
            method: 'POST',
            data: {'product_data': JSON.stringify(productData)},
            success: (function(data, status) {
                if (data.errno !== 0) {
                    ajaxErrorhandler(data.errno);
                } else {
                    flashMessage('success', 'Количката е обновена!');
                }
            }),
            fail: (function(request, status, error) {
                httpErrorHandler(request, status, error);
            })
        });
    });

    cartEmpty.click(function() {
        cartEmptyDialog.modal('show');
    });

    cartEmptyCancel.click(function() {
        cartEmptyDialog.modal('hide');
    });

    cartEmptyConfirm.click(function() {
        // Perform Ajax request and truncate table
        ajaxRequest('shopping_cart_empty', {
            params: {},
            data: {},
            method: 'GET',
            success: (function(data, status) {
                if (data.errno !== 0) {
                    ajaxErrorhandler(data.errno);
                } else {
                    $('table#_cartList').remove();
                    $('button[id*="_cartButton"]').remove();

                    flashMessage('success', 'Количката е изчистена успешно!');
                }
            }),
            fail: (function(request, status, error) { 
                httpErrorHandler(request, status, error);
            })
        });
    
        cartEmptyDialog.modal('hide');
    });
}

/**
 * Performs Ajax requst for specifed controller and return data in json format
 * 
 * @param url Id of controller to perform request with
 * @param options Options to perform request with (folling are required)
 *  type: HTTP method to use
 *  params: Parameters to pass to controller
 *  data: Data to parse in request body
 *   
 */
function ajaxRequest(url, options) {
    $.ajax(Routing.generate(url, options.params), {
        type: options.method,
        dataType: 'json',
        cache: false,
        data: options.data
    })
    .done(function(data, status) { options.success(data, status); })
    .fail(function(request, status, error) { options.fail(request, status, error); });
}

function flashMessage(type, msg) {
    text = '<div class="alert alert-' + type + '">' + msg +
           '<button type="button" class="close" data-dismiss="alert">&times;</button>' +   
           '</div>';
    
    $('.alert-' + type).remove();
    
    setTimeout(function() { 
        $('#alerts').append(text).alert();
    }, 500);
}

function ajaxErrorhandler(code) {
    flashMessage('error', 'Грешка при извличане да данните (Код: ' + code + ')');
}

function httpErrorHandler(request, status, error) {
    flashMessage('error', 'Грешка при изпълнение на HTTP заяква (Статус: ' + status + ')');
}

/*
 * 
 * End ShoppingCart functions
 * 
 * 
 */