<?php

namespace Vis\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class SecurityController extends Controller {
   
    /**
     * @Route("/login", name="admin_login")
     * @Template()
     */
    public function loginAction(Request $request) {
        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array('last_username' => $session->get(SecurityContext::LAST_USERNAME), 'error' => $error);
    }

    /**
     * @Route("/login_check", name="admin_login_check")
     */
    public function loginCheckAction() {
        
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logoutAction() {
        
    }
}

?>
