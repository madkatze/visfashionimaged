<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class UserAdmin extends Admin {
    
    protected $saltProvider;
    protected $encoderFactory;
    protected $aclProvider;
    
    protected $oldPassword;
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('username')
                ->add('realName')
                ->add('email')
                ->add('password')
                ->add('salt')
                ->add('isActive')
                ->add('roles', null, array('associated_tostring' => 'getRole'))
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('username')
                ->add('realName')
                ->add('email')
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Password again'),
                    'required' => false))
                ->add('isActive')
                ->with('Roles')
                ->add('roles', 'collection', array(
                    'type' => 'entity',
                    'options' => array(
                        'class' => 'VisFashionBundle:Role',
                        'property' => 'name',
                        'label' => 'Role'
                    ),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
            ))
        ;
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('realName')
            ->add('email')
            ->add('isActive')
            ->add('roles', null, array(), null, array(
                'property' => 'name',
                'multiple' => true,
                'expanded' => true
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('username')
            ->add('realName')
            ->add('email')
            ->add('isActive')
            ->add('password')
            ->add('salt')
        ;
    }
    
    public function prePersist($object) {
        $object->setSalt($this->saltProvider->getSalt());
        $object->setPassword($this->encoderFactory->getEncoder($object)->encodePassword($object->getPassword(), $object->getSalt()));
        parent::prePersist($object);
    }
    
    public function postPersist($object) {
        $acl = $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($object));
        $acl->insertObjectAce(UserSecurityIdentity::fromAccount($object), MaskBuilder::MASK_OWNER);
        $this->aclProvider->updateAcl($acl);
        parent::postPersist($object);
    }
    
    public function preUpdate($object) {
        // if no new password provided, set old hash
        if (\is_null($object->getPassword())) {
            $object->setPassword($this->oldPassword);
        // if password provided, persist
        } else {
            $object->setPassword($this->encoderFactory->getEncoder($object)->encodePassword($object->getPassword(), $object->getSalt()));
        }
        parent::preUpdate($object);
    }
    
    public function getFormBuilder() {
        if (!\is_null($this->getSubject()->getId())){
            $this->oldPassword = $this->getSubject()->getPassword();
            $this->formOptions = array('validation_groups' => 'user_edit');
        }
        return parent::getFormBuilder();
    }
    
    public function setSaltProvider($saltProvider) {
        $this->saltProvider = $saltProvider;
    }
    
    public function setEncoderFactory($encoderFactory) {
        $this->encoderFactory = $encoderFactory;
    }
    
    public function setAclProvider($aclProvider) {
        $this->aclProvider = $aclProvider;
    }
    
}
