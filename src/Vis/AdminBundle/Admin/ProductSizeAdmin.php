<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductSizeAdmin extends Admin {
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('producer.name')
                ->add('size')
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('producer', 'sonata_type_model_list', array(), array('property' => 'name'))
                ->add('size')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('id')
            ->add('producer.name')
            ->add('size')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('producer.name')
            ->add('size')
        ;
    }
}
