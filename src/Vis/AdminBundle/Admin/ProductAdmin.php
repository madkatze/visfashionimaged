<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class ProductAdmin extends Admin {
    
    protected $securityContext;
    protected $aclProvider;
    
    protected $oldFields;
    protected $oldVendorOwner;
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('title')
                ->add('price')
                ->add('producer.name')
                ->add('sizes', null, array('associated_tostring' => 'getSize'))
                ->add('type')
                ->add('creator.username')
                ->add('dateCreated')
                ->add('updator.username')
                ->add('dateUpdated')
                ->add('category.name')
                ->add('description')
                ->add('fields', null, array('associated_tostring' => 'getName'))
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title')
                ->add('price')
                ->add('vendor', 'sonata_type_model_list', array(), array('property' => 'title'))
                ->add('description', 'textarea', array('required' => false))
                ->add('producer', 'sonata_type_model_list', array(), array('property' => 'name'))
                ->add('sizes', 'entity', array(
                    'class' => 'Vis\FashionBundle\Entity\ProductSize',
                    'property' => 'producerAndSize',
                    'by_reference' => false,
                    'multiple' => true
                ))
                ->add('category', 'sonata_type_model_list', array(), array('property' => 'name'))
                ->add('type', 'choice', array(
                    'choices' => array('M' => 'Male', 'F' => 'Female', 'U' => 'Unisex')
                ))
                ->add('country', 'sonata_type_model_list', array(), array('property' => 'name'))
                ->with('Fields')
                ->add('fields', 'sonata_type_collection', array('by_reference' => false), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title')
            ->add('price')
            ->add('producer.name')
            ->add('category', null, array(), null, array(
                'property' => 'name'
            ))
            ->add('dateCreated', 'doctrine_orm_datetime_range')
            ->add('sizes', null, array(), null, array(
                'property' => 'size'
            ))
            ->add('vendor.title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('price')
            ->add('producer.name')
            ->add('sizes', null, array('associated_tostring' => 'getSize'))
            ->add('description')
            ->add('vendor.title')
            ->add('creator.username')
            ->add('dateCreated')
            ->add('updator.username')
            ->add('dateUpdated')
        ;
    }
    
    public function prePersist($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setCreator($user);
        $object->setUpdator($user);
        parent::prePersist($object);
    }
    
    public function postPersist($object) {
        $acl = $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($object));
        $sid = UserSecurityIdentity::fromAccount($object->getVendor()->getOwner());
        $acl->insertObjectAce($sid, MaskBuilder::MASK_OWNER);
        $this->aclProvider->updateAcl($acl);
        parent::postPersist($object);
    }
    
    public function preUpdate($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setUpdator($user);
        
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        
        // get the fields that are not passed
        $pfToRemove = \array_udiff($this->oldFields, $object->getFields(), function($a, $b) {
                    if ($a->getId() > $b->getId())
                        return 1;
                    if ($a->getId() < $b->getId())
                        return -1;
                    return 0;
                });
                
        // remove them
        foreach ($pfToRemove as $pf) {
            $em->remove($pf);
        }
        
        // give owner permissions to owner if changed and remove old owner
        $owner = $object->getVendor()->getOwner();
        if ($this->oldVendorOwner != $owner) {
            $acl = $this->aclProvider->findAcl(ObjectIdentity::fromDomainObject($object));
            $sid = UserSecurityIdentity::fromAccount($owner);
            $oldSid = UserSecurityIdentity::fromAccount($this->oldVendorOwner);

            $i = 0;
            
            foreach ($acl->getObjectAces() as $ace) {
                if ($ace->getSecurityIdentity() == $oldSid) {
                    $acl->deleteObjectAce($i);
                }
                $i++;
            }
            
            $acl->insertObjectAce($sid, MaskBuilder::MASK_OWNER);
            $this->aclProvider->updateAcl($acl);
        }
        
        parent::preUpdate($object);
    }
    
    public function getFormBuilder() {
        $this->oldFields = $this->getSubject()->getFields();
        if (!\is_null($this->getSubject()->getId())){
            $this->oldVendorOwner = $this->getSubject()->getVendor()->getOwner();
        }
        return parent::getFormBuilder();
    }
    
    public function setSecurityContext($securityContext) {
        $this->securityContext = $securityContext;
    }
    
    public function setAclProvider($aclProvider) {
        $this->aclProvider = $aclProvider;
    }
}