<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class VendorAdmin extends Admin {
    
    protected $securityContext;
    protected $aclProvider;
    
    protected $oldOwner;
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('title')
                ->add('uic')
                ->add('owner.username')
                ->add('description')
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title')
                ->add('uic')
                ->add('owner', 'entity', array('class' => 'VisFashionBundle:User', 'property' => 'username'))
                ->add('description', 'textarea', array('required' => false))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title')
            ->add('owner.username')
            ->add('uic')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('uic')
            ->add('owner.username')
            ->add('dateCreated')
            ->add('updator.username')
            ->add('dateUpdated')
        ;
    }
    
    public function prePersist($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setUpdator($user);
        parent::prePersist($object);
    }
    
    public function postPersist($object) {
        $acl = $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($object));
        $acl->insertObjectAce(UserSecurityIdentity::fromAccount($object->getOwner()), MaskBuilder::MASK_OWNER);
        $this->aclProvider->updateAcl($acl);
        parent::postPersist($object);
    }
    
    public function preUpdate($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setUpdator($user);
        
        // give owner permissions to new owner and remove old owner's permissions
        if ($this->oldOwner != $object->getOwner()) {
            $acl = $this->aclProvider->findAcl(ObjectIdentity::fromDomainObject($object));
            $sid = UserSecurityIdentity::fromAccount($object->getOwner());
            $oldSid = UserSecurityIdentity::fromAccount($this->oldOwner);

            $i = 0;
            
            foreach ($acl->getObjectAces() as $ace) {
                if ($ace->getSecurityIdentity() == $oldSid) {
                    $acl->deleteObjectAce($i);
                }
                $i++;
            }
            
            $acl->insertObjectAce($sid, MaskBuilder::MASK_OWNER);
            $this->aclProvider->updateAcl($acl);
        }
        
        parent::preUpdate($object);
    }
    
    public function getFormBuilder() {
        $this->oldOwner = $this->getSubject()->getOwner();
        return parent::getFormBuilder();
    }
    
    public function setSecurityContext($securityContext) {
        $this->securityContext = $securityContext;
    }
    
    public function setAclProvider($aclProvider) {
        $this->aclProvider = $aclProvider;
    }
}