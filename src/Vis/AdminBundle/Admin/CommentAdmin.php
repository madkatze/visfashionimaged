<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CommentAdmin extends Admin {
    
    protected $securityContext;
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('commentedEntityId')
                ->add('commentedEntityType')
                ->add('content')
                ->add('creator.username')
                ->add('dateCreated')
                ->add('updator.username')
                ->add('dateUpdated')
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('content', 'textarea')
                ->add('commentedEntityId')
                ->add('commentedEntityType')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('content')
            ->add('commentedEntityId')
            ->add('commentedEntityType')
            ->add('creator.username')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('creator.username')
            ->add('dateCreated')
            ->add('updator.username')
            ->add('dateUpdated')
            ->add('content')
        ;
    }
    
    public function prePersist($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setCreator($user);
        $object->setUpdator($user);
        parent::prePersist($object);
    }
    
    public function preUpdate($object) {
        $user = $this->securityContext->getToken()->getUser();
        $object->setUpdator($user);
        parent::preUpdate($object);
    }
    
    public function setSecurityContext($securityContext) {
        $this->securityContext = $securityContext;
    }
}