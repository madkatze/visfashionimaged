<?php

namespace Vis\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CategoryAdmin extends Admin {
    
    protected function configureShowFields(ShowMapper $filter) {
        $filter
                ->add('id')
                ->add('name')
                ->add('description')
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name')
                ->add('description', 'textarea', array('required' => false))
                ->add('parentId', 'entity', array(
                    'class' => 'VisFashionBundle:Category',
                    'property' => 'name',
                    'by_reference' => false,
                    'required' => false,
                ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('id')
        ;
    }
}

?>
