<?php

namespace Vis\FashionBundle\Form\Vendor;

use Symfony\Component\Validator\Constraints as Assert;

class Registration {
    
    /**
     * @Assert\Valid()
     */
    public $vendor;
    
    /**
     * @Assert\Valid()
     */
    public $user;
    
    /**
     * @Assert\True(message="You have to accept the terms")
     */
    public $termsAccepted;
    
}