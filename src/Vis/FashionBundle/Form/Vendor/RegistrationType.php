<?php
namespace Vis\FashionBundle\Form\Vendor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vis\FashionBundle\Form\User\UserDataType;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('vendor', new VendorType())
                ->add('user', new UserDataType())
                ->add('captcha',    'captcha')
                ->add('termsAccepted', 'checkbox')
                ->add('register', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vis\FashionBundle\Form\Vendor\Registration'
        ));
    }

    public function getName()
    {
        return 'vis_fashionbundle_vednor_registration_type';
    }
}
