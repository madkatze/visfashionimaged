<?php

namespace Vis\FashionBundle\Form\Vendor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VendorSearchType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title',  'text', array(
                        'label'     => 'Име',
                        'required'  => false
                    ))
                ->add('search', 'submit', array(
                        'label'     => 'Търсене'
                    ))
                ->setMethod('GET');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array());
    }

    public function getName() {
        return 'vis_fashionbundle_vendorsearchtype';
    }

}