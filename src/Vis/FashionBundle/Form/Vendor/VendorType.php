<?php

namespace Vis\FashionBundle\Form\Vendor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VendorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('uic')
            ->add('description', 'textarea', array('required' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vis\FashionBundle\Entity\Vendor'
        ));
    }

    public function getName()
    {
        return 'vis_fashionbundle_vendortype';
    }
}
