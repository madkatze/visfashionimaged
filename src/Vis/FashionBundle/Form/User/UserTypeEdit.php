<?php

namespace Vis\FashionBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class UserTypeEdit extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('realName', 'text', array(
                        'label' => 'Име'
                    ))
                ->add('password', 'repeated', array(
                        'type'      => 'password',
                        'required'  => false,
                        'first_options' => array('label' => 'Password'),
                        'second_options'=> array('label' => 'Password again')
                    ))
                ->add('save', 'submit', array('label' => 'Save'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class'        => 'Vis\FashionBundle\Entity\User',
            'validation_groups' => array('user_edit')
        ));
    }

    public function getName() {
        return 'vis_fashionbundle_usertype_edit';
    }

}
