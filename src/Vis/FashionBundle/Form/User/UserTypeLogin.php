<?php

namespace Vis\FashionBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserTypeLogin extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username')
                ->add('password');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
    
    }

    public function getName() {
        return 'vis_fashionbundle_usertype_login';
    }

}
