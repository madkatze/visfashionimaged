<?php

namespace Vis\FashionBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserTypeRegister extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', 'text')
                ->add('realName', 'text')
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Password again')
                ))
                ->add('email', 'email')
                ->add('captcha', 'captcha')
                ->add('termAccept', 'checkbox')
                ->add('register', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vis\FashionBundle\Entity\User'
        ));
    }

    public function getName() {
        return 'vis_fashionbundle_usertype_register';
    }

}
