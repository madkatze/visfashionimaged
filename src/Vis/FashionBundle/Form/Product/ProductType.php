<?php

namespace Vis\FashionBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title')
                ->add('price')
                ->add('description', 'textarea', array('required' => false))
                ->add('type', 'choice', array(
                    'choices' => array('M' => 'Male', 'F' => 'Female', 'U' => 'Unisex')
                ))
                ->add('fields', 'collection', array(
                    'type' => new ProductFieldType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ))
                ->add('producer', 'entity', array(
                    'empty_value' => 'Choose a producer',
                    'class' => 'VisFashionBundle:Producer',
                    'property' => 'name'
                ))
                ->add('sizes', 'entity', array(
                    'class' => 'VisFashionBundle:ProductSize',
                    'by_reference' => false,
                    'property' => 'size',
                    'expanded' => true,
                    'multiple' => true
                ))
                ->add('category', 'entity', array(
                    'class' => 'VisFashionBundle:Category',
                    'property' => 'name'
                ))
                ->add('country', 'entity', array(
                    'class' => 'VisFashionBundle:Country',
                    'property' => 'name'
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vis\FashionBundle\Entity\Product'
        ));
    }

    public function getName() {
        return 'vis_fashionbundle_producttype';
    }

}