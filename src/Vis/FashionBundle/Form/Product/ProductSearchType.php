<?php

namespace Vis\FashionBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @todo add 2 fields for price min max
 * @todo get available sizes with query to db
 */
class ProductSearchType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title',      'text', array(
                        'label'     => 'Име',
                        'required'  => false
                    ))
                ->add('price',      'hidden', array(
                        'required'  => false
                    ))
                ->add('size',       'entity', array(
                        'class'     => 'VisFashionBundle:ProductSize',
                        'property'  => 'producerAndSize',
                        'label'     => 'Размер',
                        'required'  => false,
                        'multiple'  => true,
                        'expanded'  => true
                    ))
                ->add('dateCreated','text', array(
                        'label'     => 'Добавен на',
                        'required'  => false
                    ))
                ->add('type',       'choice', array(
                        'label'         => 'Тип',
                        'choices'       => array(
                            'M'    => 'Мъжки',
                            'F'    => 'Женски'
                        ),
                        'required'      => false,
                        'multiple'      => false,
                        'empty_value'   => '<Изберете>'
                    ))
                ->add('vendor',     'entity', array(
                        'label'         => 'Търговец',
                        'class'         => 'VisFashionBundle:Vendor',
                        'property'      => 'title',
                        'multiple'      => true,
                        'expanded'      => true,
                        'required'      => false,
                        'empty_value'   => '<Изберете>'
                    ))
                ->add('category',   'entity', array(
                        'label'         => 'Категория',
                        'class'         => 'VisFashionBundle:Category',
                        'property'      => 'name',
                        'multiple'      => true,
                        'expanded'      => true,
                        'required'      => false,
                        'empty_value'   => '<Изберете>'
                    ))
                ->add('search',     'submit', array(
                        'label'         => 'Търсене'
                    ))
                ->setMethod('GET');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array());
    }

    public function getName() {
        return 'vis_fashionbundle_product_searchtype';
    }

}