<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\UserRepository")
 * @UniqueEntity(fields="username", message="Username already present in database")
 * @UniqueEntity(fields="email", message="Email already present in database")
 * 
 * @todo Implement salt getters and setters
 */
class User implements UserInterface, EquatableInterface, \Serializable {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min="4",
     *      max="32"
     * )
     * @ORM\Column(name="username", type="string", length=64)
     */
    private $username;

    /**
     * @var string
     * @Assert\Length(
     *      min="6",
     *      max="48"
     * )
     * @ORM\Column(name="realName", type="string", length=128)
     */
    private $realName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Regex(groups={"user_edit"}, pattern="/^(.*)$/")
     * @Assert\Length(
     *      min="6",
     *      max="128"
     * )
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var string
     * 
     * @ORM\Column(name="salt", type="string")
     */
    private $salt;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $roles;

    /**
     * @var boolean
     */
    private $termAccept;

    public function __construct() {
        $this->roles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set realName
     *
     * @param string $realName
     * @return User
     */
    public function setRealName($realName) {
        $this->realName = $realName;

        return $this;
    }

    /**
     * Get realName
     *
     * @return string 
     */
    public function getRealName() {
        return $this->realName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set isActive
     * @param boolean $active
     * @return User
     */
    public function setIsActive($active) {
        $this->isActive = $active;

        return $this;
    }

    /**
     * Get isActive
     * 
     * @return boolean
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * Get password salt (probably insecure)
     *  
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * Set password salt (probably insecure)
     * 
     * @return string
     */
    public function setSalt($salt) {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set whether user has accepted the agreement
     * @param boolean $accept
     */
    public function setTermAccept($accept) {
        $this->termAccept = $accept;

        return $this;
    }

    /**
     * Get whether user has accepted the agreement
     * @return boolean
     */
    public function getTermAccept() {
        return $this->termAccept;
    }

    /**
     * Get user roles
     * @return array
     */
    public function getRoles() {
        return $this->roles->toArray();
    }

    /**
     * Set user roles
     * @param ArrayCollection $roles
     */
    public function setRoles(ArrayCollection $roles) {
        
    }

    public function eraseCredentials() {
        
    }

    public function isEqualTo(UserInterface $user) {
        if (!$user instanceof WebServiceUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * Add roles
     *
     * @param \Vis\FashionBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\Vis\FashionBundle\Entity\Role $roles) {
        if (false == $this->roles->contains($roles)) {
            $this->roles[] = $roles;
        }

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Vis\FashionBundle\Entity\Role $roles
     */
    public function removeRole(\Vis\FashionBundle\Entity\Role $roles) {
        $this->roles->removeElement($roles);
    }

    /**
     * @ignore
     */
    public function serialize() {
        return serialize(array($this->id));
    }

    /**
     * @ignore
     */
    public function unserialize($serialized) {
        list ($this->id) = unserialize($serialized);
    }
    

}