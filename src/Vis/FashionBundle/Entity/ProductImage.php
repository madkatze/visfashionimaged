<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ProductImage
 *
 * @ORM\Table(name="product_images")
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\ProductImageRepository")
 */
class ProductImage {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=4096)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="file", type="string", length=255)
     * @Assert\File(maxSize="5120")
     */
    private $file;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_topmost", type="boolean")
     */
    private $isTopmost;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return ProductImage
     */
    public function setProductId($productId) {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId() {
        return $this->productId;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return ProductImage
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ProductImage
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set isTopmost
     *
     * @param boolean $isTopmost
     * @return ProductImage
     */
    public function setIsTopmost($isTopmost) {
        $this->isTopmost = $isTopmost;

        return $this;
    }

    /**
     * Get isTopmost
     *
     * @return boolean 
     */
    public function getIsTopmost() {
        return $this->isTopmost;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return ProductImage
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile() {
        return $this->file;
    }
    
    /**
     * Get absolute path to file
     * @return string
     */
    public function getAbsolutePath() {
        return $this->getPath() == null ? null : $this->getUploadRootDir() . '/' . $this->getPath();
    }
    
    /**
     * Get web based path to file
     * @return string
     */
    public function getWebPath() {
        return $this->getPath() == null ? null : $this->getUploadDir() . '/' . $this->getPath();
    }
    
    /**
     * Get file upload root directory
     */
    protected function getUploadRootDir() {
        return realpath(__DIR__ . '/../../../../web/' . $this->getUploadDir()); 
    }
    
    /**
     * Get web formatted upload dir
     */
    protected function getUploadDir() {
        return 'uploads/product_images';
    }
}