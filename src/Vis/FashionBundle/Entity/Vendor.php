<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Vendor
 *
 * @ORM\Table(name="vendors")
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\VendorRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="uic", message="UIC already present in database")
 */
class Vendor {

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Length(
     *      min="2",
     *      max="255",
     *      minMessage="Vendor's title cannot be shorter than {{ limit }}",
     *      maxMessage="Vendor's title cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Assert\Length(
     *      min="9",
     *      max="11",
     *      minMessage="UIC cannot be shorter than {{ limit }}",
     *      maxMessage="UIC cannot be longer than {{ limit }}"
     * )
     * @Assert\Regex(
     *      pattern="/^(?:BG)?[0-9]{9}$/i",
     *      message="Your UIC must be exactly 9 or 11 characters"
     * )
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $uic;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $owner;

    /**
     * @Assert\Length(
     *      max="1000",
     *      maxMessage="Description cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(type="string", length=1000, nullable=true, options={"default":" "})
     */
    private $description;

    /**
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $updator;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Vendor
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set owner
     *
     * @param \Vis\FashionBundle\Entity\User $owner
     * @return Vendor
     */
    public function setOwner(\Vis\FashionBundle\Entity\User $owner = null) {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * Set uic
     *
     * @param string $uic
     * @return Vendor
     */
    public function setUic($uic) {
        $this->uic = $uic;

        return $this;
    }

    /**
     * Get uic
     *
     * @return string 
     */
    public function getUic() {
        return $this->uic;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Vendor
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set dateCreated
     *
     * @ORM\PrePersist
     * @param \DateTime $dateCreated
     * @return Vendor
     */
    public function setDateCreated() {
        $this->dateCreated = new \DateTime();

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     * @param \DateTime $dateUpdated
     * @return Vendor
     */
    public function setDateUpdated() {
        $this->dateUpdated = new \DateTime();

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime 
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set updatedBy
     *
     * @param \Vis\FashionBundle\Entity\User $updatedBy
     * @return Vendor
     */
    public function setUpdator(\Vis\FashionBundle\Entity\User $updator = null) {
        $this->updator = $updator;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getUpdator() {
        return $this->updator;
    }

}