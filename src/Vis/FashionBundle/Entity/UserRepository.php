<?php

namespace Vis\FashionBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository implements UserProviderInterface {

    public function loadUserByUsername($username) {
        $builder = $this->createQueryBuilder('user')
                ->where('user.username = :username OR user.email = :email')
                ->setParameter('username', $username)
                ->setParameter('email', $username)
                ->getQuery();

        try {
            $user = $builder->getSingleResult();
        } catch (NoResultException $e) {
            throw new UsernameNotFoundException(
            sprintf("User with username '%s' not found!", $username)
            );
        }

        return $user;
    }

    public function refreshUser(UserInterface $user) {
        $class = get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
            sprintf("Instances of '%s' are not supported", $class)
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class) {
        return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());
    }

}

