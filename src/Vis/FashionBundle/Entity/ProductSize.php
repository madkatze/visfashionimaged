<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductSize
 *
 * @ORM\Table(name="product_size")
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\ProductSizeRepository")
 */
class ProductSize {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Producer", fetch="EAGER")
     */
    private $producer;
    
    /**
     * @ORM\ManyToMany(
     *  targetEntity="Vis\FashionBundle\Entity\Product",
     *  mappedBy="sizes"
     * )
     */
    private $products;

    /**
     * @ORM\Column(
     *  name="size", 
     *  type="string", 
     *  length=8
     * )
     * @Assert\NotNull
     * @Assert\Regex(
     *  pattern="/^(?:[LMSX3-5]{1,3}|[0-9]{1,3})$/i",
     *  message="Cloth size must be either in US or numeric format"
     * )
     */
    private $size;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set producer
     *
     * @param integer $producer
     * @return ProductSize
     */
    public function setProducer($producer) {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer
     *
     * @return integer 
     */
    public function getProducer() {
        return $this->producer;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return ProductSize
     */
    public function setSize($size) {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize() {
        return $this->size;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \Vis\FashionBundle\Entity\Product $products
     * @return ProductSize
     */
    public function addProduct(\Vis\FashionBundle\Entity\Product $products)
    {
        $this->products[] = $products;
    
        return $this;
    }

    /**
     * Remove products
     *
     * @param \Vis\FashionBundle\Entity\Product $products
     */
    public function removeProduct(\Vis\FashionBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
    
    public function getProducerAndSize() {
        
        return $this->getProducer()->getName() . ': ' . $this->getSize();
    }
}