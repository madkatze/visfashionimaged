<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductImageRepository extends EntityRepository {
    /**
     * Finds images for specifed product(s) by specifed id(s)
     * @param integer|array Product id(s) to look for
     * @return ProductImage
     */
    public function findByProductIds($productIds, $topmostOnly = false) {
        if (!is_array($productIds)) {
            $productIds = array($productIds);
        }
        
        $query  = $this->_em->createQueryBuilder();
        $expr   = $query->expr();
        
        $result = $query->select('img')
                        ->from('VisFashionBundle:ProductImage', 'img')
                        ->where($expr->andX(
                            ($topmostOnly ? $expr->eq('img.isTopmost', '1') : $expr->eq('img.isTopmost', '0')),
                            $expr->in('img.productId', ':prod_ids')
                        ))
                        ->setParameter('prod_ids', $productIds);
        
        return $result->getQuery()->execute();
    }
}