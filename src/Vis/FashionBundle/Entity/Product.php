<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\ProductRepository")
 * @Assert\Callback(methods={"isProducerContainedInSizes"})
 */
class Product {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @Assert\Length(
     *      min="1",
     *      max="255",
     *      minMessage="Product's title cannot be shorter than {{ limit }}",
     *      maxMessage="Product's title cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Assert\GreaterThan(value=0)
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @Assert\Length(
     *      max="1000",
     *      maxMessage="Description cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(type="string", length=1000, nullable=true, options={"default":" "})
     */
    private $description;

    /**
     * @ORM\ManyToMany(
     *  targetEntity="Vis\FashionBundle\Entity\ProductSize",
     *  inversedBy="products"
     * )
     * @ORM\JoinTable(name="products_sizes")
     * @Assert\Count(min = "1", minMessage="You must select at least one size")
     * @Assert\Valid
     */
    private $sizes;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Vendor")
     */
    private $vendor;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Category")
     * @Assert\Valid
     */
    private $category;

    /**
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;

    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $updator;
    
    /**
     * @Assert\Choice(choices={"M", "F", "U"})
     * @ORM\Column(name="type", type="string", length=1)
     */
    private $type;
    
    /**
     * @ORM\OneToMany(
     *  targetEntity="Vis\FashionBundle\Entity\ProductField",
     *  cascade={"persist", "remove"},
     *  mappedBy="product"
     * )
     * @Assert\Valid
     */
    private $fields;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Producer")
     * @Assert\Valid
     */
    private $producer;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\Country")
     */
    private $country;
    
    public function isProducerContainedInSizes(ExecutionContextInterface $context) {
        
        $producer = $this->getProducer();
        
        if(!\is_null($producer)) {
            
            foreach($this->getSizes()->toArray() as $s) {
                if ($s->getProducer() != $producer) {
                    $context->addViolationAt('sizes', 'You have selected a size, not belonging to the specified producer');
                    return false;
                }
            }
        }
    }

    /**
     * @ignore
     */
    public function __construct() {
        $this->fields   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sizes    = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set creator
     *
     * @param \Vis\FashionBundle\Entity\User $creator
     * @return Product
     */
    public function setCreator(\Vis\FashionBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set vendor
     *
     * @param \Vis\FashionBundle\Entity\Vendor $vendor
     * @return Product
     */
    public function setVendor(\Vis\FashionBundle\Entity\Vendor $vendor = null) {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * Get vendor
     *
     * @return \Vis\FashionBundle\Entity\Vendor 
     */
    public function getVendor() {
        return $this->vendor;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set dateCreated
     *
     * @ORM\PrePersist
     * @param \DateTime $dateCreated
     * @return Vendor
     */
    public function setDateCreated() {
        $this->dateCreated = new \DateTime();

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     * @param \DateTime $dateUpdated
     * @return Vendor
     */
    public function setDateUpdated() {
        $this->dateUpdated = new \DateTime();

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime 
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * @return User
     */
    public function setUpdator(\Vis\FashionBundle\Entity\User $updator = null) {
        $this->updator = $updator;

        return $this;
    }

    /**
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getUpdator() {
        return $this->updator;
    }

    /**
     * Get type
     * 
     * @return string
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param string $type
     * @return Product
     */
    public function setType($type) {
        $this->type = $type;
        
        return $this;
    }

    /**
     * Add fields
     *
     * @param \Vis\FashionBundle\Entity\ProductField $field
     * @return Product
     */
    public function addField(\Vis\FashionBundle\Entity\ProductField $field) {
        $field->setProduct($this);
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param \Vis\FashionBundle\Entity\ProductField $fields
     */
    public function removeField(\Vis\FashionBundle\Entity\ProductField $field) {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     */
    public function getFields() {
        return $this->fields->toArray();
    }

    /**
     * Set category
     *
     * @param \Vis\FashionBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Vis\FashionBundle\Entity\Category $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Vis\FashionBundle\Entity\Category 
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set producer
     *
     * @param \Vis\FashionBundle\Entity\Producer $producer
     * @return Product
     */
    public function setProducer(\Vis\FashionBundle\Entity\Producer $producer = null) {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer
     *
     * @return \Vis\FashionBundle\Entity\Producer 
     */
    public function getProducer() {
        return $this->producer;
    }

    /**
     * Add size
     *
     * @param \Vis\FashionBundle\Entity\ProductSize $size
     * @return Product
     */
    public function addSize(\Vis\FashionBundle\Entity\ProductSize $size) {
        $this->sizes[] = $size;
        $size->addProduct($this);

        return $this;
    }

    /**
     * Remove size
     *
     * @param \Vis\FashionBundle\Entity\ProductSize $size
     */
    public function removeSize(\Vis\FashionBundle\Entity\ProductSize $size) {
        $this->sizes->removeElement($size);
        $size->removeProduct($this);
    }

    /**
     * Get sizes
     */
    public function getSizes()
    {
        return $this->sizes;
    }
     
    /**
     * Set country
     *
     * @param \Vis\FashionBundle\Entity\Country $country
     * @return Product
     */
    public function setCountry(\Vis\FashionBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Vis\FashionBundle\Entity\Country 
     */
    public function getCountry() {
        return $this->country;
    }
    
}