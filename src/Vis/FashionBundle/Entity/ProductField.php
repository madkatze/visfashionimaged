<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductsFields
 *
 * @ORM\Table(name="products_fields")
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\ProductFieldRepository")
 */
class ProductField {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *  targetEntity="Vis\FashionBundle\Entity\Product",
     *  inversedBy="fields"
     * )
     */
    private $product;

    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;
    
    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(name="data", type="string", length=128)
     */
    private $data;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return ProductsFields
     */
    public function setProduct($productId) {
        $this->product = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductsFields
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set data
     * 
     * @param sring field data
     * @return ProductsFields
     */
    public function setData($data) {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * Get data
     * 
     * @return string
     */
    public function getData() {
        return $this->data;
    }
}