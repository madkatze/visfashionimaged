<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CommentRepository
 */
class CommentRepository extends EntityRepository
{
    public function findCommentsByEntity($entity) {
        
        $entityType = Comment::getMappingForEntity($entity);
        $qb = $this->_em->createQueryBuilder();
        $qb->select('c')
           ->from('VisFashionBundle:Comment', 'c')
           ->where($qb->expr()->andX(
                        $qb->expr()->eq('c.commentedEntityId', ':id'),
                        $qb->expr()->eq('c.commentedEntityType', ':type')))
           ->orderBy('c.dateCreated', 'DESC')
           ->setParameter('id', $entity->getId())
           ->setParameter('type', $entityType);
        
        return $qb;
    }
    
    public function getCommentsCountByEntityAndCreator($entity, $creator) {
        
        $qb = $this->findCommentsByEntity($entity);
        $qb->andWhere($qb->expr()->eq('c.creator', ':creator_id'))
            ->select($qb->expr()->count('c'))
            ->setParameter('creator_id', $creator->getId());
        
        return $qb->getQuery()->getSingleScalarResult();
    }
}
