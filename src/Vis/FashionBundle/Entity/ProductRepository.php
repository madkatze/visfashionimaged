<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr;

/**
 * ProductRepository
 */
class ProductRepository extends EntityRepository {
    
    /**
     * Gets products for vendor
     * 
     * @param Vendor $vendor
     */
    public function findProductsByVendor($vendor) {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
                ->from('VisFashionBundle:Product', 'p')
                ->where($qb->expr()->eq('p.vendor', ':vendor'))
                ->setParameter('vendor', $vendor->getId());

        return $qb;
    }
    
    /**
     * Gets information for products with specifed id(s)
     * 
     * @param int|array Product id(s)
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function findProductsByIds($prodIds) {
        $productIds = (is_array($prodIds) ? $prodIds : array($prodIds));
        $qb = $this->_em->createQueryBuilder();
        
        $qb ->select('p')
            ->from('VisFashionBundle:Product', 'p')
            ->where($qb->expr()->in('p.id', ':prod_ids'))
            ->setParameter('prod_ids', $productIds);
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * Gets products sorted
     * 
     * @param type $sort
     * @param type $order
     */
    public function findProductsSortedBy($sort = null, $order = null) {
        
        $qb = $this->_em->createQueryBuilder();

        $qb->select('p')
                ->from('VisFashionBundle:Product', 'p')
                ->orderBy($sort, $order);

        return $qb;
    }
    
    /**
     * Gets products by criteria
     * 
     * @param array(
     *  'title' => string, 
     *  'price' => array('price_low', 'price_high'),
     *  'sizes' => ArrayCollection of ProductSize,
     *  'modified' => DateTime,
     *  'type' => string,
     *  'vendors' => ArrayCollection of Vendor,
     *  'categories' => ArrayCollection of Category
     * ) $criteria
     */
    public function findProductsBy($criteria) {
        
        if (empty($criteria)) return array();
        
        $qb = $this->_em->createQueryBuilder();

        $qb->select('prod', 'pf', 'ps')
                ->from('VisFashionBundle:Product', 'prod')
                ->leftJoin('prod.sizes', 'ps')
                ->leftJoin('prod.fields', 'pf');
        
        $expr = $qb->expr();

        // title
        if (!empty($criteria['title'])) {
            $qb->andWhere('prod.title LIKE :prod_title');

            $qb->setParameter('prod_title', '%' . $criteria['title'] . '%', Type::STRING);
        }

        // price
        if (!empty($criteria['price'])) {
            $qb->andWhere($expr->andX(
                            $expr->gte('prod.price', ':price_low'), $expr->lte('prod.price', ':price_high')
                    ));
            $qb->setParameter('price_low', $criteria['price']['price_low'], Type::FLOAT);
            $qb->setParameter('price_high', $criteria['price']['price_high'], Type::FLOAT);
        }

        // sizes
        if (!empty($criteria['sizes'])) {
            $sizesIds = array();
            foreach ($criteria['sizes']->toArray() as $sizesData) {
                $sizesIds[] = intval($sizesData->getId());
            }
            
            if (!empty($sizesIds)) {
                $qb->andWhere($expr->andX($expr->in('ps.id', ':prod_sizes')));

                $qb->setParameter('prod_sizes', $sizesIds);
            }
        }

        // modifiedAfter
        if (!empty($criteria['modified'])) {
            $qb->andWhere($expr->andX($expr->gte('prod.dateUpdated', ':prod_date')));

            $date = \DateTime::createFromFormat('d.m.Y', $criteria['modified']);

            $qb->setParameter('prod_date', $date->format('Y-n-d H:i:s'));
        }

        // type
        if (!empty($criteria['type'])) {
            $qb->andWhere($expr->andX($expr->eq('prod.type', ':prod_type')));

            $qb->setParameter('prod_type', $criteria['type'], Type::TEXT);
        }

        // vendors
        if (!empty($criteria['vendors'])) {
            $vendorIds = array();
            foreach ($criteria['vendors']->toArray() as $vendorData) {
                $vendorIds[] = intval($vendorData->getId());
            }

            if (!empty($vendorIds)) {
                $qb->andWhere($expr->andX($expr->in('prod.vendor', ':prod_vendor')));

                $qb->setParameter('prod_vendor', $vendorIds);
            }
        }

        // categories
        if (!empty($criteria['categories'])) {
            $categoryIds = array();
            foreach ($criteria['categories']->toArray() as $catData) {
                $categoryIds[] = intval($catData->getId());
            }

            if (!empty($categoryIds)) {
                $catQb = $this->_em->createQueryBuilder();
                
                $catQb  ->select('cat.id')
                        ->from('VisFashionBundle:Category', 'cat')
                        ->where($expr->in('cat.parentId', ':prod_pcat'));
                
                $pCatsArr = $catQb->getQuery()->execute(array(
                    'prod_pcat' => $categoryIds
                ));
                $pCats = array();
                
                foreach ($pCatsArr as $pCatData) {
                    $pCats[] = $pCatData['id'];
                }
                
                $qb->andWhere($expr->in('prod.category', ':prod_cat'));

                $qb->setParameter('prod_cat', $categoryIds + $pCats);
            }
        }
        
        return $qb;
    }
    
    /**
     * Gets the price interval of the products
     * 
     * @return array (1 => 'min-value', 2 => 'max-value')
     * 
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getPriceInterval() {
        
        $qb = $this->_em->createQueryBuilder();
        
        $qb->select($qb->expr()->min('p.price'), $qb->expr()->max('p.price'))
                ->from('VisFashionBundle:Product', 'p');
        
        return $qb->getQuery()->getSingleResult();
    }
    
}
