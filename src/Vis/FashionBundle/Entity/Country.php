<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="Id already present in database")
 */
class Country {

    /**
     * @Assert\Length(
     *      max="2",
     *      maxMessage="Country's id cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(name="id", type="string", length=2)
     * @ORM\Id
     */
    private $id;

    /**
     * @Assert\Length(
     *      max="64",
     *      maxMessage="Country's name cannot be longer than {{ limit }}"
     * )
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * Set id
     *
     * @param string $id
     * @return Country
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

}