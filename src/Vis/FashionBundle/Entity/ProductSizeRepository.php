<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ProductSizeRepository
 */
class ProductSizeRepository extends EntityRepository {
    
    public function findSizesByProducer($producer) {
        
        $qb = $this->_em->createQueryBuilder();
        
        $qb->select('s')
                ->from('VisFashionBundle:ProductSize', 's')
                ->where($qb->expr()->eq('s.producer', ':producer'))
                ->setParameter('producer', $producer->getId());
        
        return $qb;
    }
}
