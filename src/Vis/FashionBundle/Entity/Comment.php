<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="comments")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Vis\FashionBundle\Entity\CommentRepository")
 */
class Comment
{
    /**
     * Not persisted
     */
    static $mapping = array(
        'Vis\FashionBundle\Entity\Vendor' => 1,
        'Vis\FashionBundle\Entity\Product' => 2
    );
    
    /**
     * Not persisted
     */
    private $entity;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="commented_entity_id", type="integer")
     */
    private $commentedEntityId;
    
    /**
     * @ORM\Column(name="commented_entity_type", type="integer")
     */
    private $commentedEntityType;
    
     /**
     * @Assert\Length(
     *      max="500",
     *      maxMessage="Content cannot be longer than {{ limit }}"
     * )
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="string", length=500)
     */
    private $content;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $creator;
    
    /**
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vis\FashionBundle\Entity\User")
     */
    private $updator;
    
     /**
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentedEntityId
     *
     * @param integer $commentedEntityId
     * @return Comment
     */
    public function setCommentedEntityId($commentedEntityId)
    {
        $this->commentedEntityId = $commentedEntityId;
    
        return $this;
    }

    /**
     * Get commentedEntityId
     *
     * @return integer 
     */
    public function getCommentedEntityId()
    {
        return $this->commentedEntityId;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set dateCreated
     *
     * @ORM\PrePersist
     * @param \DateTime $dateCreated
     * @return Comment
     */
    public function setDateCreated()
    {
        $this->dateCreated = new \DateTime();
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $dateUpdated
     * @return Comment
     */
    public function setDateUpdated()
    {
        $this->dateUpdated = new \DateTime();
    
        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime 
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set creator
     *
     * @param \Vis\FashionBundle\Entity\User $creator
     * @return Comment
     */
    public function setCreator(\Vis\FashionBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;
    
        return $this;
    }

    /**
     * Get creator
     *
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set updator
     *
     * @param \Vis\FashionBundle\Entity\User $updator
     * @return Comment
     */
    public function setUpdator(\Vis\FashionBundle\Entity\User $updator = null)
    {
        $this->updator = $updator;
    
        return $this;
    }

    /**
     * Get updator
     *
     * @return \Vis\FashionBundle\Entity\User 
     */
    public function getUpdator()
    {
        return $this->updator;
    }

    /**
     * Set commentedEntityType
     *
     * @param integer $commentedEntityType
     * @return Comment
     */
    public function setCommentedEntityType($commentedEntityType)
    {
        $this->commentedEntityType = $commentedEntityType;
    
        return $this;
    }

    /**
     * Get commentedEntityType
     *
     * @return integer 
     */
    public function getCommentedEntityType()
    {
        return $this->commentedEntityType;
    }
    
    public function getEntity() {
        return $this->entity;
    }
    
    public function setEntity($entity) {
        $this->entity = $entity;
        // it needs getId
        $this->setCommentedEntityId($entity->getId());
        $this->setCommentedEntityType(self::getMappingForEntity($entity));
    }
    
    static function getMappingForEntity($entity) {
        
        return self::$mapping[\get_class($entity)];
    }
}