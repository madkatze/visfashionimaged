<?php

namespace Vis\FashionBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VendorRepository
 */
class VendorRepository extends EntityRepository {
    
    public function findVendors() {
        
        $qb = $this->_em->createQueryBuilder();
        
        $qb->select('v')
           ->from('VisFashionBundle:Vendor', 'v');
        
        return $qb;
    }
}
