<?php

namespace Vis\FashionBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class FashionExceptionListener {
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        
        if ($exception instanceof \UnexpectedValueException) {
            $response = new Response();
            $message = 'You sent some interesting arguments';
            $response->setContent($message);
            $event->setResponse($response);
        }
    }
}