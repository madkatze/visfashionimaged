<?php

namespace Vis\FashionBundle\Service;

use Doctrine\ORM\EntityNotFoundException;

class ShoppingCart {
    /**
     * Session handler
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;
    
    /**
     * Doctrine ORM entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    public function __construct($entityManager, $sessionHandler) {
        $this->session = $sessionHandler;
        $this->entityManager = $entityManager;
    }
    
    /**
     * Add product to shopping cart or changes it's quantity if already exists
     * 
     * @param integer $prodId
     * @param integer $quantity
     * 
     * @return \Vis\FashionBundle\Service\ShoppingCart
     * 
     * @throws EntityNotFoundException Product not found
     */
    public function add($prodId, $size, $quantity = 1) {
        if ($this->entityManager->getRepository('VisFashionBundle:Product')->find($prodId) == null) {
            throw new EntityNotFoundException;
        }
        
        $prodData = new \stdClass();
        $cartData = $this->session->get('shopping_cart', array());
        
        $prodData->size     = $size;
        $prodData->quantity = $quantity;
        
        if (!isset($cartData[$prodId])) {
            $cartData[$prodId] = array($prodData);
        } else {
            $prodFound = false;
            
            foreach ($cartData[$prodId] as $sizeIndex => $sizeData) {
                if ($sizeData->size == $size) {
                    $cartData[$prodId][$sizeIndex] = $prodData;
                    
                    $prodFound = true;
                }
            }
            
            if (!$prodFound) {
                $cartData[$prodId][] = $prodData;
            }
        }
        
        return $this->session->set('shopping_cart', $cartData);
    }
    
    /**
     * Return quantity of product for specifed size
     * 
     * @param integer $prodId Product id
     * @param mixed $size Product size
     * 
     * @return integer
     */
    public function count($prodId = null, $size = null) {
        $prodData = $this->session->get('shopping_cart', null);
        
        if ($prodData == null || !isset($prodData[$prodId])) {
            return 0;
        }
        
        $count = 0;
        
        foreach ($prodData as $pId => $pData) {
            foreach ($pData as $sizeData) {
                if ($pId == $prodId && $size == $sizeData->size) {
                    // Size matches (return quantity for specifed product with specifed size)
                    $count += $sizeData->quantity;
                } elseif ($prodId == $pId) {
                    // Product matches (return quantity for specifed product regreadless of the size)
                    $count += $sizeData->quantity;
                } else {
                    // Nothing matches (return quantity for every product in cart)
                    $count += $sizeData->quantity;
                }
            }
        }
        
        return $count;
    }
    
    /**
     * Delete product with specifed id from the cart
     * 
     * @param integer $prodId Product id
     * @return boolean
     */
    public function del($prodId, $size = null) {
        $prodData = $this->session->get('shopping_cart', null);
        
        if ($prodData == null || !isset($prodData[$prodId])) {
            return false;
        }
        
        if ($size == null) {
            // No size is specifed, delete everything regarding this product
            unset($prodData[$prodId]);
        } else {
            foreach ($prodData[$prodId] as $pIndex => $pData) {
                if ($pData->size == $size) {
                    unset($prodData[$prodId][$pIndex]);
                }
            }
        }
        
        $this->session->set('shopping_cart', $prodData);
        
        return true;
    }
    
    /**
     * Get list of product(s) in cart
     * 
     * @return array 
     */
    public function getList() {
        return $this->session->get('shopping_cart', array());
    }
    
    /**
     * Empty shopping cart
     * 
     * @return boolean
     */
    public function truncate() {
        return $this->session->remove('shopping_cart');
    }
}