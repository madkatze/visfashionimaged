<?php

namespace Vis\FashionBundle\Service;

/**
 * Interface for the salt provider service
 */
interface SaltProviderInterface {
    
    public function getSalt();
}
