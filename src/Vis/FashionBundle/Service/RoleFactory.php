<?php
namespace Vis\FashionBundle\Service;

use Vis\FashionBundle\Entity\Role;

class RoleFactory {
    
    private $entityManager;
    
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }
    
    /**
     * Gets role for specified type of member.
     * @param string $roleType Type of member to get role for (e.g. user, vendor, anonymous, admin)
     */
    public function getRole($roleType) {
        return $this->getRoleData($roleType);
    }
    
    /**
     * Retrieve data for specifed role or create it if doesn't exists
     * @param int $roleType Role type to retrieve data for
     * @todo Add support for config checks to make sure, that specifed role exists
     */
    private function getRoleData($roleType) {
        $roleName = null;
        $roleData = null;
        
        switch ($roleType) {
            case 'user':
                $roleName = 'ROLE_USER';
                $roleData = 'ROLE_USER';
                break;
                
            case 'vendor':
                $roleName = 'ROLE_VENDOR_ADMIN';
                $roleData = 'ROLE_VENDOR_ADMIN';
                break;
                
            case 'admin':
                $roleName = 'ROLE_SUPER_ADMIN';
                $roleData = 'ROLE_SUPER_ADMIN';
                break;
            
            default:
                throw new Exception("Specifed role doesn't exists!");
        }
                
        $doctrine   = $this->entityManager;
        $role       = $doctrine ->getRepository('VisFashionBundle:Role')
                                ->findOneBy(array('role' => $roleName));
        
        if ($role == null) {
            $newRole = new Role();
            
            $newRole->setName($roleName);
            $newRole->setRole($roleData);
            
            $doctrine->persist($newRole);
            $doctrine->flush();
        } else {
            $newRole = $role;
        }
       
        return $newRole;
    }
}