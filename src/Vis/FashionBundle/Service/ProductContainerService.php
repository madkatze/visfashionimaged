<?php

namespace Vis\FashionBundle\Service;

use Vis\FashionBundle\Entity\Product;

class ProductContainerService {
    
    protected $name;
    protected $repository;
    protected $session;
    
    public function __construct($name, $session, $repository) {
        
        $this->name = $name;
        $this->session = $session;
        $this->repository = $repository;
    }
    
    public function add(Product $product) {
        
        $this->addId($product->getId());
    }
    
    public function remove(Product $product) {
        
        $this->removeId($product->getId());
    }
    
    public function getProducts() {
        
        $ids = $this->getIds();
        if (empty($ids)) return array();
        return $this->repository->findBy(array('id' => $ids));
    }
    
    public function setIds($ids) {
        
        $this->session->set($this->name, $ids);
    }
    
    public function getIds() {
        
        return $this->session->get($this->name, array());
    }
    
    protected function addId($id) {
        
        $ids = $this->getIds();
        $ids[$id] = $id;
        $this->setIds($ids);
    }
    
    protected function removeId($id) {
        
        $ids = $this->getIds();
        if (isset($ids[$id])) unset($ids[$id]);
        $this->setIds($ids);
    }
}