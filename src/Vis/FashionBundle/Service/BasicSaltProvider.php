<?php

namespace Vis\FashionBundle\Service;

/**
 * We got the idea for this salt from FOSUserBundle
 * Provides better security (and flawless work) than Symfony Security random bytes
 */
class BasicSaltProvider implements SaltProviderInterface {
    
    public function getSalt() {
        
        return \base_convert(\sha1(\uniqid(\mt_rand(), true)), 16, 36);
    }
    
}
