<?php

namespace Vis\FashionBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception as SecurityCoreException;

use Doctrine\ORM;

use Vis\FashionBundle\Entity\User;
use Vis\FashionBundle\Form\User as UserForm;

/**
 * @Route("/user")
 */
class UserController extends Controller {

    /**
     * Error(s) occured during form validation
     * @var array
     */
    private $errors = array();

    /**
     * @ignore
     */
    public function __call($method, $args) {
        $this->errors = array();
    }

    /**
     * @Route("/login", name="user_login")
     * @Template("VisFashionBundle:User:login.html.twig")
     * @Method({"GET", "POST"})
     * 
     * @todo Add support for anti-bot mechanism (captcha) if user failed
     * to login after n number of tries.
     */
    public function loginAction(Request $request) {
        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array('last_username' => $session->get(SecurityContext::LAST_USERNAME), 'error' => $error);
    }

    /**
     * @Route("/login_check", name="user_login_check")
     */
    public function loginCheckAction() {
        
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logoutAction() {
        
    }

    /**
     * @Route("/edit", name="user_edit_profile")
     * @Template("VisFashionBundle:User:edit.html.twig")
     * @Method({"GET", "POST"})
     */
    public function editAction() {
        $user = $this->getUser();
        
        if (!$user) {
            return $this->redirect($this->generateUrl('user_login'));
        }
        
        $secContext = $this->get('security.context');
        $encFactory = $this->get('security.encoder_factory');
        $flashBag   = $this->get('session')->getFlashBag();
        $roleFactory= $this->get('role_factory');
        
        $form       = $this->createForm(new UserForm\UserTypeEdit(), $user, array(
            'action' => $this->generateUrl('user_edit_profile'),
            'method' => 'POST'   
        ));
        $entityMan  = $this->getDoctrine()->getManager();
        $userData   = $this->userExists($user->getId());
        
        if (!$userData || !$user->getIsActive()) {
            throw new SecurityCoreException\AccessDeniedException();
        }
        
        if ((   $secContext->isGranted($roleFactory->getRole('user')->getName())        ||
                $secContext->isGranted($roleFactory->getRole('vendor')->getName() ))    && 
                $secContext->isGranted('EDIT', $user)) {
            if ($this->getRequest()->isMethod('POST')) {
                /*
                 * Password fields may be left empty during profile update.
                 * In such a case, consider storing old password and write it 
                 * back to database when edit is done.
                 */
                $oldPassword = $user->getPassword();
                $form->handleRequest($this->getRequest());
                
                if ($form->isValid()) {
                    if ($user->getPassword() != null) {
                        /*
                         * User password has been modified
                         */
                        $user->setPassword(
                            $encFactory->getEncoder($user)->encodePassword(
                                    $user->getPassword(), $user->getSalt()
                        ));
                    } else {
                        /*
                         * User password is same again 
                         */
                        $user->setPassword($oldPassword);
                    }
                    
                    $entityMan->persist($user);
                    $entityMan->flush();
                    
                    $flashBag->add('notice', 'Редакцията е успешна!');
                } else {
                    foreach ($form->getErrors() as $fError) {
                        $flashBag->add('error', $fError);
                    }
                }
            }
        } else {
            throw new SecurityCoreException\AccessDeniedException();
        }
        
        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/reset", name="user_reset")
     * @Template("VisFashionBundle:User:recovery.html.twig")
     * @Method({"GET", "POST"})
     */
    public function resetPasswordAction(Request $request) {

    }
    
    /**
     * @ignore
     */
    private function userExists($id) {
        $userData = $this   ->getDoctrine()
                            ->getManager()
                            ->getRepository('VisFashionBundle:User')
                            ->find($id);
                
        if ($userData) {
            return $userData;
        }
        
        return false;
    }

}
