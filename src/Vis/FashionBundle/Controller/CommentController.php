<?php

namespace Vis\FashionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Vis\FashionBundle\Entity\Product;
use Vis\FashionBundle\Entity\Vendor;
use Vis\FashionBundle\Form\CommentType;
use Vis\FashionBundle\Entity\Comment;

/**
 * @Route("/comment")
 */
class CommentController extends Controller
{
    const MAX_COUNT_PER_ENTITY = 2;
    
    /**
     * @Route("/product/{id}/new", name="product_comment_create")
     * @Method("POST")
     */
    public function createForProductAction(Product $product) {
        
        // check user's role
        if (false == $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        return $this->createCommentHelper($product);
    }
    
    /**
     * @Route("/vendor/{id}/new", name="vendor_comment_create")
     * @Method("POST")
     */
    public function createForVendorAction(Vendor $vendor) {
        
        // check user's role
        if (false == $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        return $this->createCommentHelper($vendor);
    }
   
    /**
     * @Route("/{id}/edit", name="comment_update")
     * @Method("POST")
     */
    public function updateAction(Comment $comment) {
        
        // check user's role
        if (false == $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        // check if granted edit on entity
        if (false == $this->get('security.context')->isGranted('EDIT', $comment)) {
            throw new AccessDeniedException();
        }
        
        $flashes = $this->get('session')->getFlashBag();
        $form = $this->createForm(new CommentType(), $comment);
        $form->handleRequest($this->getRequest());
        
        if ($form->isValid()) {
           $comment->setUpdator($this->getUser());
           $em = $this->getDoctrine()->getManager();
           $em->persist($comment);
           
           $em->flush();
           
           $flashes->add('notice', 'Comment updated successfully');
        } else {
           $flashes->add('notice', $form->getErrorsAsString());
        }
        
        $referer = $this->getRequest()->headers->get('referer');
        
        if (!$referer) return new Response();
        
        return $this->redirect($referer);
    }
    
    /**
     * Performs comment creation
     */
    protected function createCommentHelper($entity) {
        
        $user = $this->getUser();
        $aclProvider = $this->get('security.acl.provider');
        $flashes = $this->get('session')->getFlashBag();
        $referer = $this->getRequest()->headers->get('referer');
        
        $form = $this->createForm(new CommentType());
        $form->handleRequest($this->getRequest());
        $comment = $form->getData();
        
        if ($form->isValid()) {
            // check comments count before adding
            // probably we need some kind of a pesimistic lock here
            // but a captcha in the form should do the job
            // i'm not gonna test this as it never goes to production anyway
            $commentsCount = $this->getDoctrine()->getRepository('VisFashionBundle:Comment')
                    ->getCommentsCountByEntityAndCreator($entity, $user);

            if ($commentsCount >= self::MAX_COUNT_PER_ENTITY) {
                $flashes->add('notice', 'Cannot add more than ' . self::MAX_COUNT_PER_ENTITY . ' comments per entity');

                return $this->redirect($referer);
            }
        
            $comment->setEntity($entity);
            $comment->setCreator($user);
            $comment->setUpdator($user);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            
            $em->flush();
            
            // give user owner permissions to the comment
            $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($comment));
            $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);
            
            $flashes->add('notice', 'Comment added successfully');
        } else {
            $flashes->add('notice', $form->getErrorsAsString());
        }
        
        if (!$referer) return new Response();
        
        return $this->redirect($referer);
    }
}