<?php

namespace Vis\FashionBundle\Controller\VendorAdmin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Vis\FashionBundle\Entity\Product;
use Vis\FashionBundle\Entity\Vendor;
use Vis\FashionBundle\Form\Product\ProductType;

// currently for checking if a user can create an obj,
// we don't use the acl
// we may, later on, use some class scope ace
// with role security identity
// depending on project specifications(???)

// FixMe: choose better route?
/**
 * @Route("/vendor/{vendor_id}/product")
 * @ParamConverter("vendor", class="VisFashionBundle:Vendor", options={"id" = "vendor_id"})
 */
class ProductController extends Controller
{
    static $notOwnerAccError = "You are not the owner to add products here";
    
    /**
     * @Route("/", name="vendor_products")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Vendor $vendor) {
        
        $productsQb = $this->getDoctrine()->getRepository('VisFashionBundle:Product')->findProductsByVendor($vendor);
        
        $pagination = $this->get('knp_paginator')->paginate(
                $productsQb,
                $this->getRequest()->get('page', 1),
                $this->getRequest()->get('ppc', 10)
        );
        
        return array('vendor' => $vendor, 'pagination' => $pagination);
    }
    
    /**
     * @Route("/new", name="vendor_product_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Vendor $vendor) {
        
        // check if granted create
        if ($vendor->getOwner() != $this->getUser()) {
            throw new AccessDeniedException(self::$notOwnerAccError);
        }
        
        
        $form = $this->createForm(new ProductType(), new Product(abe), array(
            'action' => $this->generateUrl('vendor_product_create', array('vendor_id' => $vendor->getId())),
            'method' => 'POST'));
        
        return array('vendor' => $vendor, 'form' => $form->createView());
    }
    
    /**
     * @Route("/new", name="vendor_product_create")
     * @Method("POST")
     * @Template("VisFashionBundle:VendorAdmin/Product:new.html.twig")
     */
    public function createAction(Vendor $vendor) {
        
        $user = $this->getUser();
        
        // check if granted create
        if ($vendor->getOwner() != $this->getUser()) {
            throw new AccessDeniedException(self::$notOwnerAccError);
        }
        
        $product = new Product();
        $form = $this->createForm(new ProductType(), $product, array(
            'action' => $this->generateUrl('vendor_product_create', array('vendor_id' => $vendor->getId())),
            'method' => 'POST'));
       
        $form->handleRequest($this->getRequest());
        
        if ($form->isValid()) {
            $product->setCreator($user);
            $product->setUpdator($user);
            $product->setVendor($vendor);
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            // give owner permissions for this product to user
            $aclProvider = $this->get('security.acl.provider');
            $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($product));
            $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);
            
            $this->get('session')->getFlashBag()->add('notice', 'Product created successfully');
            
            return $this->redirect($this->generateUrl('vendor_product_show', array('vendor_id' => $vendor->getId(),
                                                                                   'id' => $product->getId())));
        }
        
        return array('vendor' => $vendor, 'form' => $form->createView());
    }
    
    /**
     * @Route("/{id}", name="vendor_product_show")
     * @ParamConverter("product", class="VisFashionBundle:Product", options={"mapping" = {"id": "id", "vendor": "vendor"}})
     * @Method("GET")
     * @Template()
     */
    public function showAction(Vendor $vendor, Product $product) {
        
        $commentsQb = $this->getDoctrine()->getRepository('VisFashionBundle:Comment')->findCommentsByEntity($product);
        
        $comments = $this->get('knp_paginator')->paginate(
                $commentsQb,
                $this->getRequest()->get('page', 1)
        );
        
        return array('vendor' => $vendor, 'product' => $product, 'comments' => $comments);
    }
    
    /**
     * @Route("/{id}/edit", name="vendor_product_edit")
     * @ParamConverter("product", class="VisFashionBundle:Product", options={"mapping" = {"id": "id", "vendor": "vendor"}})
     * @Method("GET")
     * @Template()
     */
    public function editAction(Vendor $vendor, Product $product) {
        
        // check if granted edit
        if(false == $this->get('security.context')->isGranted('EDIT', $product)) {
            throw new AccessDeniedException();
        }
        
        $form = $this->createForm(new ProductType(), $product, array(
            'action' => $this->generateUrl('vendor_product_update', array('vendor_id' => $vendor->getId(),
                                                                          'id' => $product->getId())),
            'method' => 'POST'
        ));
        
        return array('product' => $product, 'form' => $form->createView());
    }
    
    /**
     * @Route("/{id}/edit", name="vendor_product_update")
     * @ParamConverter("product", class="VisFashionBundle:Product", options={"mapping" = {"id": "id", "vendor": "vendor"}})
     * @Method("POST")
     * @Template("VisFashionBundle:VendorAdmin/Product:edit.html.twig")
     */
    public function updateAction(Vendor $vendor, Product $product) {
        
        // check if granted edit
        if(false == $this->get('security.context')->isGranted('EDIT', $product)) {
            throw new AccessDeniedException();
        }
        
        $productFields = $product->getFields();
        
        $form = $this->createForm(new ProductType(), $product, array(
            'action' => $this->generateUrl('vendor_product_update', array('vendor_id' => $vendor->getId(),
                                                                          'id' => $product->getId())),
            'method' => 'POST'
        ));
        
        $form->handleRequest($this->getRequest());
        
        if ($form->isValid()) {
            $product->setUpdator($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            
            // get the products that are not passed
            $pfToRemove = \array_udiff($productFields, $product->getFields(),
                                       function($a, $b) { if($a->getId() > $b->getId()) return 1;
                                                          if($a->getId() < $b->getId()) return -1;
                                                          return 0;
                                       });
            // remove them
            foreach($pfToRemove as $pf) {
                $em->remove($pf);
            }
            
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('notice', 'Edited successfully');
        }
        
        return array('product' => $product, 'form' => $form->createView());
    }
    
    /**
     * @Route("/{id}/delete", name="vendor_product_delete")
     * @ParamConverter("product", class="VisFashionBundle:Product", options={"mapping" = {"id": "id", "vendor": "vendor"}})
     * @Method("POST")
     */
    public function deleteAction(Vendor $vendor, Product $product) {
        
        // check if granted delete
        if(false == $this->get('security.context')->isGranted('DELETE', $product)) {
            throw new AccessDeniedException();
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();
        
        // Let the comments be gone :)
        $em ->createQuery("DELETE FROM VisFashionBundle:Comment comm WHERE comm.commentedEntityId = :comm_id AND comm.commentedEntityType = :comm_type")
            ->setParameters(array(
                'comm_id'   => $product->getId(),
                'comm_type' => \Vis\FashionBundle\Entity\Comment::getMappingForEntity($product)
            ))
            ->execute();
        
        $this->get('session')->getFlashBag()->add('notice', 'Product deleted successfully');
        
        return $this->redirect($this->generateUrl('vendor_products', array('vendor_id' => $vendor->getId())));
    }
}