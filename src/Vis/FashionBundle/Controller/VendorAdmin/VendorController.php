<?php

namespace Vis\FashionBundle\Controller\VendorAdmin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Vis\FashionBundle\Form\Vendor\VendorType;
use Vis\FashionBundle\Form\Vendor\Registration;
use Vis\FashionBundle\Form\Vendor\RegistrationType;
use Vis\FashionBundle\Entity\Role;
use Vis\FashionBundle\Entity\Vendor;

/**
 * @Route("/vendor")
 */
class VendorController extends Controller
{
    const MAX_ITEMS_PER_PAGE = 30;
    const COMMENTS_PER_PAGE = 5;
    
    /**
     * @Route("/", name="vendor_index")
     * @Template()
     */
    public function indexAction() {
        
        $vendor = $this->getDoctrine()->getRepository('VisFashionBundle:Vendor')->findOneBy(array('owner' => $this->getUser()));
        
        return array('vendor' => $vendor);
    }
    
    /**
     * @Route("/{id}", name="vendor_show", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction(Vendor $vendor) {
        
        $em = $this->getDoctrine()->getManager();
        
        $commentsQb = $em->getRepository('VisFashionBundle:Comment')->findCommentsByEntity($vendor);
        
        $comments = $this->get('knp_paginator')->paginate(
                $commentsQb,
                $this->getRequest()->get('page', 1),
                self::COMMENTS_PER_PAGE
        );
        
        return array('vendor' => $vendor, 'comments' => $comments);
    }
    
    /**
     * @Route("/{id}/edit", name="vendor_edit", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function editAction(Vendor $vendor) {
        
        $form = $this->createForm(new VendorType(), $vendor,
                array('action' => $this->generateUrl('vendor_update',
                                                     array('id' => $vendor->getId())),
                      'method' => 'POST'));
        
        // check if granted edit on entity
        if (false == $this->get('security.context')->isGranted('EDIT', $vendor)) {
            throw new AccessDeniedException();
        }
        
        return array('vendor' => $vendor, 'editForm' => $form->createView());
    }
    
    /**
     * @Route("/{id}", name="vendor_update", requirements={"id" = "\d+"})
     * @Method("POST")
     * @Template("VisFashionBundle:VendorAdmin/Vendor:edit.html.twig")
     */
    public function updateAction(Vendor $vendor) {
        
        // check if granted edit on entity
        if (false == $this->get('security.context')->isGranted('EDIT', $vendor)) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(new VendorType(), $vendor,
                array('action' => $this->generateUrl('vendor_update',
                                                     array('id' => $vendor->getId())),
                      'method' => 'POST'));

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $vendor->setUpdator($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($vendor);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Vendor successfully edited!');
            
            return $this->redirect($this->generateUrl('vendor_show', array('id' => $vendor->getId())));
        }

        return array('vendor' => $vendor, 'editForm' => $form->createView());
    }
    
    /**
     * @Route("/list", name="vendor_list")
     * @Template()
     */
    public function listAction() {
        
        $vendorsQb = $this->getDoctrine()->getRepository('VisFashionBundle:Vendor')->findVendors();

        $pagination = $this->get('knp_paginator')->paginate(
                $vendorsQb,
                $this->getRequest()->get('page', 1),
                \min($this->getRequest()->get('ppc', self::MAX_ITEMS_PER_PAGE), self::MAX_ITEMS_PER_PAGE)
        );
        
        return array('pagination' => $pagination);
    }
}
