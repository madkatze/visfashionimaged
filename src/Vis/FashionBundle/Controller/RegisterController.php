<?php

namespace Vis\FashionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Vis\FashionBundle\Form\Vendor\VendorType;
use Vis\FashionBundle\Form\Vendor\Registration;
use Vis\FashionBundle\Form\Vendor\RegistrationType;
use Vis\FashionBundle\Entity\Role;
use Vis\FashionBundle\Entity\Vendor;
use Vis\FashionBundle\Entity\User;
use Vis\FashionBundle\Form\User as UserForm;

/**
 * Front-end registration controller. Just keep it for easy routing
 * if required.
 */
class RegisterController extends Controller {

    /**
     * @Route("/register", name="register")
     * @Template()
     */
    public function indexAction() {
        return $this->render("VisFashionBundle:Register:index.html.twig");
    }

    /**
     * @Route("/register-vendor", name="vendor_register")
     * @Template()
     */
    public function registerVendorAction() {
        $reg = new Registration();

        $form = $this->createForm(new RegistrationType(), $reg);

        $request = $this->getRequest();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $aclProvider = $this->get('security.acl.provider');

                $em = $this->getDoctrine()->getManager();
                $user = $reg->user;
                $reg->vendor->setOwner($user);
                $reg->vendor->setUpdator($user);
                
                // perform standard user registration with ROLE_VENDOR_ADMIN
                $this->setPasswordSaltAndActivate($user);
                $role = $this->get('role_factory')->getRole('vendor');
                $user->addRole($role);

                $em->persist($reg->vendor);
                $em->persist($user);

                $em->flush();

                // give the user owner permissions to himself
                $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($user));
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_OWNER);
                $aclProvider->updateAcl($acl);

                // give owner permissions to the vendor
                $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($reg->vendor));
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_OWNER);
                $aclProvider->updateAcl($acl);

                $this->get('session')->getFlashBag()->add('notice', 'Registration successful!');

                return $this->redirect($this->generateUrl('user_login'));
            }
        }

        return array('registrationForm' => $form->createView());
    }

    /**
     * @Route("/register-user", name="user_register")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function registerUserAction(Request $request) {
        $user = new User();
        $form = $this->createForm(new UserForm\UserTypeRegister(), $user, array(
            'action' => $this->generateUrl('user_register'),
            'method' => 'POST'
        ));
        $aclProvider = $this->get('security.acl.provider');

        if ($request->isMethod('POST') && $form->handleRequest($request)) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                
                // preform standart user registration with ROLE_USER
                $this->setPasswordSaltAndActivate($user);
                $user->addRole($this->get('role_factory')->getRole('user'));
                $em->persist($user);
                $em->flush();

                //give the user owner permissions to himself
                $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($user));
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_OWNER);
                $aclProvider->updateAcl($acl);

                $this->get('session')->getFlashBag()->add('notice', 'Registration successful!');

                $this->redirect($this->generateUrl('_home'));
            }
        }
        
        return array('form' => $form->createView());
    }
    
    /**
     * This is a helper method that extracts some code to avoid duplication
     * 
     * It's better than a copy/paste, thought it is not a good solution
     */
    protected function setPasswordSaltAndActivate($user) {
        $encFactory = $this->get('security.encoder_factory');
        $saltProvider = $this->get('fashion.salt_provider');
        
        $user->setSalt($saltProvider->getSalt());

        /*
         * Rehash the password filled by Form handler 
         */
        $user->setPassword(
                $encFactory->getEncoder($user)
                        ->encodePassword(
                                $user->getPassword(), $user->getSalt()
                        )
        );
        
        $user->setIsActive(true);
    }

}
