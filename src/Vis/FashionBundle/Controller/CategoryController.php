<?php

namespace Vis\FashionBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/category", name="category_index")
 */
class CategoryController extends Controller {
    
    /**
     * @ignore
     */
    private $roleHandler;
    
    /**
     * @ignore
     */
    private $entityManager;
    
    /**
     * @ignore
     */
    private $secContext;
    
    /**
     * @ignore
     */
    private $userData;
    
    /**
     * @Route("/")
     * @Route("/index", name="_categories")
     * @Template("VisFashionBundle:Category:index.html.twig")
     */
    public function indexAction() {
        return $this->redirect($this->generateUrl('category_list'));
    }

    /**
     * @Route("/__list_cats")
     * @Template()
     */
    public function listAction() {
        $this->loadServices();
        
        $data = $this   ->entityManager
                        ->getManager()
                        ->getRepository('VisFashionBundle:Category')
                        ->findAll();
        
        if ($data == null) {
            $catData = array();
        } else {
            foreach ($data as $parentData) {
                $cData = array();
                
                if ($parentData->getParentId() == null) {
                    $cData['id']    = $parentData->getId();
                    $cData['name']  = $parentData->getName();
                    
                    $cData['childs'] = array();
                    
                    foreach ($data as $childData) {
                        if ($childData->getParentId() == null) {                          
                            continue;
                        } else {
                            if ($parentData->getId() == $childData->getParentId()) {
                                $cData['childs'][] = array(
                                    'id'    => $childData->getId(),
                                    'name'  => $childData->getName(),
                                );
                            }
                        }
                    }
                } else {
                    continue;
                }
                
                $catData[] = $cData;
            }
        }
        
        return $this->render('VisFashionBundle:Category:catSideList.html.twig', array(
            'cat_data' => $catData
        ));
    }

    /**
     * @Route("/list_category/{catId}",
     *  name="category_list",
     *  defaults={"catId" = 0}
     * )
     * @Template("VisFashionBundle:Category:listCategory.html.twig")
     */
    public function listSubcatAction($catId) {
        $this->loadServices();
        
        $catCrit = sprintf("%s %s",
                ($catId > 0 ? '='       : 'LIKE'),
                ($catId > 0 ? ':cat_id' : "'%'")
        );
        $catQuery = $this   ->entityManager
                            ->getManager()
                            ->createQuery("SELECT cat FROM VisFashionBundle:Category cat WHERE cat.id {$catCrit}");
     
        if ($catId > 0) {
            $catQuery->setParameters(array(
                'cat_id' => $catId
            ));
        }
        
        $catData = $catQuery->getResult();
        
        if ($catData == null) {
            $catData = array();
        }
        
        return array(
            'categories' => $catData
        );
    }
    
    /**
     * @Route("/list_product/{catId}",
     *  name="category_list_product",
     *  defaults={"catId" = 0}
     * )
     * @Template("VisFashionBundle:Category:listProduct.html.twig")
     */
    public function listProductAction($catId) {
        $this->loadServices();
        
        $catProducts = $this->entityManager
                            ->getManager()
                            ->createQuery('
                                    SELECT prod FROM VisFashionBundle:Product prod WHERE prod.category = :cat_id
                                    OR prod.category IN (SELECT cat.id FROM VisFashionBundle:Category cat WHERE cat.parentId = :pcat_id)
                                    ORDER BY prod.category
                                ')
                            ->setParameters(array(
                                'cat_id'    => $catId,
                                'pcat_id'   => $catId
                            ));
        
        $products = $this->get('knp_paginator')->paginate(
                $catProducts, $this->getRequest()->get('page', 1), 32
        );
        
        return array(
            'products' => $products
        );
    }
    
    /**
     * @ignore
     */
    private function loadServices() {
        if (!isset($this->roleHandler)) {
            $this->roleHandler      = $this->get('role_factory');
            $this->secContext       = $this->get('security.context');

            $this->entityManager    = $this->getDoctrine();
            $this->userData         = $this->getUser();
        }
    }
}