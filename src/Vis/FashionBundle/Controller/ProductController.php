<?php

namespace Vis\FashionBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use Vis\FashionBundle\Entity\Product;
use Vis\FashionBundle\Entity\Comment;
use Vis\FashionBundle\Entity\Producer;
use Vis\FashionBundle\Form\CommentType;

/**
 * Product controller.
 *
 * @Route("/product")
 */
class ProductController extends Controller {

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/show/{id}", name="product_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Product $product) {
        $em = $this->getDoctrine()->getManager();
        
        $commentsQb = $em->getRepository('VisFashionBundle:Comment')->findCommentsByEntity($product);
        
        $comments = $this->get('knp_paginator')->paginate(
                $commentsQb,
                $this->getRequest()->get('page', 1)
        );
        
        $commentForm = $this->createForm(new CommentType(), new Comment(), array(
            'action' => $this->generateUrl('product_comment_create', array('id' => $product->getId())),
            'method' => 'POST'
        ));
        
        return array(
            'product' => $product,
            'comments' => $comments,
            'commentForm' => $commentForm->createView()
        );
    }
    
    /**
     * @Route("/compare", name="product_compare")
     * @Method("GET")
     * @Template()
     */
    public function compareAction() {
        
        $products = $this->get('fashion.product_container')->getProducts();
        
        return array('products' => $products);
    }
    
    /**
     * @Route("/compare/{id}/add", name="product_compare_add")
     * @Method("GET")
     */
    public function addToCompareAction(Product $product) {
        
        $this->get('fashion.product_container')->add($product);
        
        $referer = $this->getRequest()->headers->get('referer');
        
        if (!$referer) return new Response();
        
        return $this->redirect($referer);
    }
    
    /**
     * @Route("/compare/{id}/remove", name="product_compare_remove")
     * @Method("GET")
     */
    public function removeFromCompareAction(Product $product) {
        
        $this->get('fashion.product_container')->remove($product);
        
        $referer = $this->getRequest()->headers->get('referer');
        
        if (!$referer) return new Response();
        
        return $this->redirect($referer);
    }
    
    /**
     * @Route("/size/{id}", name="sizes_by_producer", defaults={"id"="0"})
     */
    public function sizesByProducerAction(Producer $producer) {
        
        $sizesQb = $this->getDoctrine()->getRepository('VisFashionBundle:ProductSize')->findSizesByProducer($producer);
        
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('products', 'producer'));
        
        $serializer = new Serializer(array($normalizer), array('json' => new JsonEncoder()));
        $json = $serializer->serialize($sizesQb->getQuery()->getResult(), 'json');
        
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
}