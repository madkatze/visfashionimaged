<?php

namespace Vis\FashionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vis\FashionBundle\Form\Product\ProductSearchType;

class HomeController extends Controller {

    /**
     * @Route("/index")
     * @Route("/", name="_home")
     */
    public function indexAction() {
        
        $form = $this->createForm(new ProductSearchType(), null, array(
            'action' => $this->generateUrl('search_results_product'),
            'method' => 'GET'
        ));
        
        $productsQb = $this->getDoctrine()->getRepository('VisFashionBundle:Product')->findProductsSortedBy('p.dateUpdated', 'DESC');
        
        $pagination = $this->get('knp_paginator')->paginate(
                $productsQb,
                $this->getRequest()->get('page', 1), 12
        );

        return $this->render("VisFashionBundle:Home:index.html.twig", array(
            'product_search' => $form->createView(),
            'pagination' => $pagination
        ));
    }

}