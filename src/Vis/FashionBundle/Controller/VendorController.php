<?php

namespace Vis\FashionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vis\FashionBundle\Entity\Vendor;
use Vis\FashionBundle\Entity\Comment;
use Vis\FashionBundle\Form\CommentType;

/**
 * @Route("/v")
 */
class VendorController extends Controller
{
    /**
     * @Route("/{id}", name="v_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Vendor $vendor) {
        $em = $this->getDoctrine()->getManager();
        
        $commentsQb = $em->getRepository('VisFashionBundle:Comment')->findCommentsByEntity($vendor);
        
        $comments = $this->get('knp_paginator')->paginate(
                $commentsQb,
                $this->getRequest()->get('page', 1)
        );
        
        $commentForm = $this->createForm(new CommentType(), new Comment(), array(
            'action' => $this->generateUrl('vendor_comment_create', array('id' => $vendor->getId())),
            'method' => 'POST'
        ));
        
        return array(
            'vendor' => $vendor,
            'comments' => $comments,
            'commentForm' => $commentForm->createView()
        );
    }
}
