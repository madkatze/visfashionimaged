<?php

namespace Vis\FashionBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


/**
 * General description of error codes returned by this controller for Ajax requests
 * 
 * +--------+------------------------------------------------------------+----------+
 * | errno  | description                                                | status   |
 * +--------+------------------------------------------------------------+----------+
 * | 0      | No error                                                   | in use   |
 * | 1      | Removing products from cart when already empty             | in use   |
 * | 2      | Removing products causing quantity to goes negative number | not used |
 * | 3      | Product doesn't exists                                     | in use   |
 * | 4      | Product not in cart                                        | in use   |
 * | 5      | Product information is invalid                             | in use   |
 * +--------+------------------------------------------------------------+----------+
 */

/**
 * @Route("/cart")
 */
class CartController extends Controller {

    /**
     * @Route("/", name="shopping_cart_index")
     * @Route("/index")
     * @Template("VisFashionBundle:Cart:index.html.twig")
     */
    public function indexAction() {
        $entityMan  = $this->getDoctrine()->getManager()->getRepository('VisFashionBundle:Product');
        $cartHndl   = $this->get('shopping_cart');
        $productList= $cartHndl->getList();
        
        $productIds = array();
        $productData= array();
        $grandTotal = 0.0;

        if (!empty($productList)) {
            foreach ($productList as $prodId => $prodData) {
                $productIds[] = $prodId;
            }
        }
        
        if (!empty($productIds)) {
            $productMatch = $entityMan->findProductsByIds($productIds);

            $findProductId = (function($productId) use ($productMatch) {
                foreach ($productMatch as $prodData) {
                    if ($prodData->getId() == $productId) {
                        return $prodData;
                    }
                }

                return false;
            });

            foreach ($productList as $index => $data) {
                $prodData = $findProductId($index);

                if ($prodData) {
                    foreach ($data as $sCartProduct) {
                        $productData[] = array(
                            'id'        => $prodData->getId(),
                            'name'      => $prodData->getTitle(),
                            'quantity'  => $sCartProduct->quantity,
                            'size'      => $sCartProduct->size,
                            'price_unit'=> $prodData->getPrice(),
                            'price'     => $sCartProduct->quantity * $prodData->getPrice()
                        );

                        $grandTotal += $sCartProduct->quantity * $prodData->getPrice();
                    }
                }
            }
        } else  {
            $this->get('session')->getFlashBag()->add('error', 'Количката е празна!');
        }
        
        return $this->render('VisFashionBundle:Cart:index.html.twig', array(
            'productData'   => $productData,
            'grandTotal'    => $grandTotal
        ));
    }
    
    /**
     * @Route("/update", name="shopping_cart_update")
     * @Method({"POST"})
     * @Template()
     */
    public function updateAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $encoder    = new JsonEncoder();
        $cartHndl   = $this->get('shopping_cart');
        $cartData   = $encoder->decode($this->getRequest()->request->get('product_data', array()), 'json');
        $response   = null;
        
        foreach ($cartData as $prodData) {
            if (!$cartHndl->add(
                $prodData['id'],
                $prodData['size'],
                $prodData['quantity']
            )) {
                $response = array(
                    'errno' => 5
                );
                
                break;
            }
        }
        
        if ($response !== null) {
            $response = array(
                'errno' => 0
            );
        }
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type' => 'application/json'
        ));
    }
    
    /**
     * @Route("/add/{productId}",name="shopping_cart_add")
     * @Method({"POST"})
     * @Template()
     */
    public function addAction($productId) {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $cartHndl = $this->get('shopping_cart');
        $cartData = array(
            'id'        => $productId,
            'size'      => $this->getRequest()->request->get('size'),
            'quantity'  => intval($this->getRequest()->request->get('quantity'))
        );
        $response   = null;
        
        $cartCount = $cartHndl->count($cartData['id'], $cartData['size']);
        
        if ($cartCount == 0 && $cartData['quantity'] <= 0) {
            // Cannot distract from cart, when product count is 0
            $response = array('errno' => 1);
        } else {
            try {
                $cartHndl->add($cartData['id'], $cartData['size'], $cartData['quantity']);
                $response = array(
                    'errno'     => 0,
                    'quantity'  => $cartData['quantity']
                );
            } catch (Doctrine\ORM\EntityNotFoundException $e) {
                // Product doesn't exists
                $response = array('errno' => 3);
            }
        }
        
        $encoder = new JsonEncoder();
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type' => 'application/json'
        ));
    }
    
    /**
     * @Route("/delete/{productId}/{size}", name="shopping_cart_del")
     * @Method({"GET"})
     * @Template()
     */
    public function delAction($productId, $size = null) {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $cartHndl = $this->get('shopping_cart');
        $response = null;
        
        if ($cartHndl->del($productId, $size)) {
            $response = array(
                'errno' => 0
            );
        } else {
            // Product not found in cart
            $response = array(
                'errno' => 4
            );
        }
        
        $encoder = new JsonEncoder();
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type' => 'application/json'
        ));
    }
    
    /**
     * @Route("/list", name="shopping_cart_list")
     * @Method({"GET"})
     * @Template()
     */
    public function listAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $cartHndl = $this->get('shopping_cart');
        $prodData = $cartHndl->getList();
        
        $response = array(
            'errno'     => 0,
            'products'  => array()
        );

        foreach ($prodData as $pId => $pData) {
            $sizeData = array(
                'prodId'    => $pId,
                'sizes'     => array()
            );

            foreach ($pData as $pSizeData) {
                $sizeData['sizes'][] = array($pSizeData->size => $pSizeData->quantity);
            }

            $response['products'][] = $sizeData;
        }
        
        $encoder = new JsonEncoder();
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type' => 'application/json'
        ));
    }
    
    /**
     * @Route("/count", name="shopping_cart_count")
     * @Method({"GET"})
     * @Template()
     */
    public function countAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $cartHndl = $this->get('shopping_cart');
        $response = array(
            'errno' => 0,
            'count' => $cartHndl->count()
        );
        $encoder  = new JsonEncoder();
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type'  => 'application/json'
        ));
    }
    
    
    /**
     * @Route("/cart/empty", name="shopping_cart_empty")
     * @Method({"GET"})
     * @Template()
     */
    public function emptyAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return new Response('', 400);
        }
        
        $cartHndl = $this->get('shopping_cart');
        $cartHndl->truncate();
        
        $response = array('errno' => 0);
        
        $encoder = new JsonEncoder();
        
        return new Response($encoder->encode($response, 'json'), 200, array(
            'Content-Type' => 'application/json'
        ));
    }
}