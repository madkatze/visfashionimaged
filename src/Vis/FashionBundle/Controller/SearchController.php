<?php

namespace Vis\FashionBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Types\Type;
use Vis\FashionBundle\Form\Product\ProductSearchType;
use Vis\FashionBundle\Form\Vendor\VendorSearchType;

/**
 * @Route("/search")
 */
class SearchController extends Controller {

    /**
     * @Route("/", name="search_index")
     */
    public function indexAction() {
        return new Response();
    }
    
    /**
     * @Route("-product", name="search_product")
     * @Method({"GET"})
     * @Template()
     */
    public function searchProductAction() {

        $form = $this->createForm(new ProductSearchType(), null, array(
            'action' => $this->generateUrl('search_results_product'),
            'method' => 'GET'
        ));
        
        $priceInt = $this->getDoctrine()->getRepository('VisFashionBundle:Product')->getPriceInterval();

        return array(
            'form'      => $form->createView(), 
            'products'  => array(),
            'price'     => array(
                'min'       => round($priceInt[1], 0, PHP_ROUND_HALF_DOWN),
                'max'       => round($priceInt[2], 0, PHP_ROUND_HALF_UP),
                'step'      => 1,
                'current'   => sprintf("[%d, %d]", 
                        round($priceInt[1], 0, PHP_ROUND_HALF_DOWN), 
                        round($priceInt[2], 0, PHP_ROUND_HALF_UP)
                    )
                )
        );
    }

    /**
     * @Route("-results-vendor", name="search_results_vendor")
     * @Route("-vendor", name="search_vendor")
     * @Method({"GET"})
     * @Template("VisFashionBundle:Search:searchVendor.html.twig")
     */
    public function searchResultsVendorAction(Request $req) {
        $form = $this->createForm(new VendorSearchType());
        $flashBag = $this->get('session')->getFlashBag();

        $form->handleRequest($req);
        
        $formData = $form->getData();
        
        $query = $this->getDoctrine()->getRepository('VisFashionBundle:Vendor')->findVendors();
        
        if (!empty($formData)) {
            $expr = $query->expr();

            if (!empty($formData['title'])) {
                $query->where($expr->like('v.title', ':vendor_title'));

                $query->setParameter('vendor_title', '%' . $formData['title'] . '%', Type::STRING);
            }
        }

        $data = $this->get('knp_paginator')->paginate(
            $query, $req->get('page', 1), 12
        );

        if (empty($data)) {
            $flashBag->add('error', 'Няма намерени съвпадения!');
        }

        return array(
            'form' => $form->createView(),
            'pagination' => $data
        );
    }

    /**
     * @Route("-results-product", name="search_results_product")
     * @Method({"GET"})
     */
    public function searchResultsProductAction(Request $req) {
        $form = $this->createForm(new ProductSearchType());
        $pager = $this->get('knp_paginator');
        $flashBag = $this->get('session')->getFlashBag();

        $form->handleRequest($req);

        $respData = array();
        $formData = $form->getData();
        
        $criteria = array(
            'title'     => (!empty($formData['title'])          ? $formData['title']        : ''),
            'sizes'     => (!empty($formData['size'])           ? $formData['size']         : ''),
            'modified'  => (!empty($formData['dateCreated'])    ? $formData['dateCreated']  : ''),
            'type'      => (!empty($formData['type'])           ? $formData['type']         : ''),
            'vendors'   => (!empty($formData['vendor'])         ? $formData['vendor']       : ''),
            'categories'=> (!empty($formData['category'])       ? $formData['category']     : ''),
        );

        if (!empty($formData['price'])) {
            $prices = explode(',', $formData['price']);

            if (count($prices) == 2) {
                $criteria['price'] = array('price_low' => $prices[0], 'price_high' => $prices[1]);
            }
        }

        $productsQb = $this->getDoctrine()->getRepository('VisFashionBundle:Product')->findProductsBy($criteria);
        $priceInt   = $this->getDoctrine()->getRepository('VisFashionBundle:Product')->getPriceInterval();
                
        $data = $pager->paginate($productsQb, $req->get('page', 1), $req->get('ppage', 32));

        if (count($data) > 0) {
            $respData['products']   = $data;
            $respData['pagination'] = $data;
        } else {
            $flashBag->add('error', 'Няма намерени продукти!');
        }
        
        $respData['form']       = $form->createView();
        $respData['price']      = array(
                'min'       => round($priceInt[1], 0, PHP_ROUND_HALF_DOWN),
                'max'       => round($priceInt[2], 0, PHP_ROUND_HALF_UP),
                'step'      => 1,
                'current'   => sprintf("[%d, %d]", 
                        round($priceInt[1], 0, PHP_ROUND_HALF_DOWN), 
                        round($priceInt[2], 0, PHP_ROUND_HALF_UP)
                    )
            );

        return $this->render('VisFashionBundle:Search:searchProduct.html.twig', $respData);
    }

}